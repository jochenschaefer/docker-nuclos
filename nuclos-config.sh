#!/bin/bash

echo "Configure Nuclos..." >> $NUCLOS_HOME/logs/server.log

# Nuclos tomcat directory
TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)

# Copy Nuclos client to data volume. Sharing the client dir directly causes an error during Nuclos update.
echo "Copy client..." >> $NUCLOS_HOME/logs/server.log
rsync -av --del $NUCLOS_HOME/client/ $NUCLOS_DIR/client/

echo "Copy templates..." >> $NUCLOS_HOME/logs/server.log
rm -f $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF
rm -f $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
rm -f $NUCLOS_STARTUP
cp $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF-template $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF
cp $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF-template $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
cp $NUCLOS_STARTUP-template $NUCLOS_STARTUP

# Update webclient port in configs
if [ -n "$WEBCLIENT_PORT" ]; then
	if ! [ $NUCLOS_PORT == $WEBCLIENT_PORT ]; then
		echo "Set weclient port..." >> $NUCLOS_HOME/logs/server.log
		sed -i 's|:'"$NUCLOS_PORT"'\/|:'"$WEBCLIENT_PORT"'/|g' "$TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF"
	fi
fi

# Update total memory RAM usage
if [ -n "$TOTAL_RAM_GB" ]; then
	echo "Set heap size..." >> $NUCLOS_HOME/logs/server.log
	export RAM=$(($TOTAL_RAM_GB*1024))
	sed -i 's:-'"Xmx$NUCLOS_HEAPSIZE"'m:-Xmx'"$RAM"'m:g' $NUCLOS_STARTUP
fi

# HTTPS keystore installation
if [ -f $NUCLOS_DIR/keystore ]; then
	echo "Set SSL..." >> $NUCLOS_HOME/logs/server.log
	export KEYSTORE_FILE=$NUCLOS_HOME/extra/.keystore
	cp $NUCLOS_DIR/keystore $KEYSTORE_FILE
	# Nuclos default generates warning "This server is vulnerable to a BEAST attack"
	#sed -i 's|<Connector.*/>|<Connector SSLEnabled=\"true\" clientAuth=\"false\" compressableMimeType=\"text/html,text/xml,text/js\" compression=\"on\" compressionMinSize=\"2048\" keystoreFile=\"'"$KEYSTORE_FILE"'\" keystorePass=\"'"$KEYSTORE_PASSWORD"'\" maxThreads=\"150\" port=\"80\" protocol=\"HTTP/1.1\" scheme=\"https\" secure=\"true\" sslProtocol=\"TLS\"/>|' $TOMCAT_DIR/conf/server.xml
	sed -i 's|<Connector.*/>|<Connector SSLEnabled=\"true\" clientAuth=\"false\" compressableMimeType=\"text/html,text/xml,text/js\" compression=\"on\" compressionMinSize=\"2048\" keystoreFile=\"'"$KEYSTORE_FILE"'\" keystorePass=\"'"$KEYSTORE_PASSWORD"'\" maxThreads=\"1000\" port=\"80\" protocol=\"HTTP/1.1\" scheme=\"https\" secure=\"true\" sslProtocol=\"TLS\" sslEnabledProtocols=\"TLSv1,TLSv1.1,TLSv1.2\" useServerCipherSuitesOrder=\"true\" ciphers=\"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_DHE_RSA_WITH_AES_128_CBC_SHA256,TLS_DHE_RSA_WITH_AES_128_CBC_SHA,TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,TLS_DHE_RSA_WITH_AES_256_CBC_SHA,TLS_RSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA,TLS_RSA_WITH_AES_256_CBC_SHA\"/>|' $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF
	# add "-Djdk.tls.ephemeralDHKeySize=2048" to CATALINA_OPTS
	sed -i 's:JAVA_OPTS=":JAVA_OPTS="-Djdk.tls.ephemeralDHKeySize=2048 :g' $NUCLOS_STARTUP
fi

# Install extensions
nuclos-extensions.sh

# Default db schema is 'nuclos'
export DB_SCHEMA=${DB_SCHEMA:-'nuclos'}

# Set db configuration
echo "Set DB connection..." >> $NUCLOS_HOME/logs/server.log
cd $NUCLOS_CONF
rm -f server.properties
rm -f jdbc.properties
cp $NUCLOS_DIR/server.properties-template server.properties
cp $NUCLOS_DIR/jdbc.properties-template jdbc.properties
sed -i 's:DB_HOST:'"postgres"':g' jdbc.properties
sed -i 's:DB_PORT:'"$POSTGRES_PORT_5432_TCP_PORT"':g' jdbc.properties
sed -i 's:DB_DATABASE:'"$POSTGRES_ENV_POSTGRES_DB"':g' jdbc.properties
sed -i 's:DB_USER:'"$POSTGRES_ENV_POSTGRES_USER"':g' jdbc.properties
sed -i 's:DB_PASSWORD:'"$POSTGRES_ENV_DB_PASSWORD"':g' jdbc.properties
sed -i 's:DB_SCHEMA:'"$DB_SCHEMA"':g' server.properties

# Set live search index
if [ "$LIVE_SEARCH" == "true" ]; then
    sed -i 's:nuclos.index.path=off:'"nuclos.index.path=$NUCLOS_HOME/data/index"':g' server.properties
fi

# DEV Environment
if [ "$DEVELOPMENT" == "true" ]; then
	echo "Enable development..." >> $NUCLOS_HOME/logs/server.log
	sed -i 's|JAVA_OPTS="|JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address='"$NUCLOS_DEBUG_PORT"' |g' $NUCLOS_STARTUP
	sed -i 's|environment.development=false|environment.development=true|g' server.properties
	sed -i 's|environment.production=true|environment.production=false|g' server.properties
fi