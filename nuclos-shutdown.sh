#!/bin/bash

echo "Shut down Nuclos..." >> $NUCLOS_HOME/logs/server.log

rm -f $NUCLOS_HOME/logs/server*

$NUCLOS_HOME/bin/shutdown.sh
sleep 10
pkill -9 -f java