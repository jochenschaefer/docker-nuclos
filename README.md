# Nuclos Dockerfile

This repository contains **Dockerfile** of [Nuclos](https://www.nuclos.de/) for [Docker](https://www.docker.com/)'s [automated build](https://registry.hub.docker.com/u/nuccess/nuclos/) published to the public [Docker Hub Registry](https://registry.hub.docker.com/).

## Base Docker Image

* [nuccess/nuclos-jdk](https://hub.docker.com/r/nuccess/nuclos-jdk/)

## Docker Tags

Visit [Tags](https://hub.docker.com/r/nuccess/nuclos/tags/)

## Installation

1. Install [Docker](https://www.docker.com/).

2. Download [automated build](https://registry.hub.docker.com/u/dockerfile/java/) from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull nuccess/nuclos-db`

   (alternatively, you can build an image from Dockerfile: `docker build -t="nuccess/nuclos-db" https://bitbucket.org/nuccess/docker-nuclos-db.git`)


---

# Usage

You can affect the behavior of the container with a variety of environment variables and volumes. The most important are described below. **A running Nuclos DB container named nuclos-db-9.6 is required for all the examples given here.** Local directory examples */Users/Maik/Nuclos/Docker/...* must be replaced.

## Development Environment  
###### for the latest Nuclos version
This is a good start for a default development environment.

```shell
docker run --rm -d nuccess/nuclos --name nuclos-dev-417 -p 8417:80 -e WEBCLIENT_PORT=8417  
-e DB_SCHEMA=dev417 -e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8  
--volumes-from nuclos-db-9.6 --link nuclos-db-9.6:POSTGRES
```

Here you will find advanced options for easy access to logs `/opt/nuclos/home/logs`, generated Java source files `/opt/nuclos/home/data/codegenerator` and a Java debug port `8000`:

```shell
docker run --rm -d nuccess/nuclos --name nuclos-dev-417 -p 8417:80 -p 18417:8000 -e WEBCLIENT_PORT=8417  
-e DB_SCHEMA=dev417  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-codegen:/opt/nuclos/home/data/codegenerator  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-devlogs:/opt/nuclos/home/logs  
-e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8  
--volumes-from nuclos-db-9.6 --link nuclos-db-9.6:POSTGRES
```

## Production Environment  
###### for Nuclos v4.17.1
For a productive environment it is recommended to place the document attachments outside the container. An update of the Nuclos version thus only consists of an replace of the Nuclos container, without having to migrate the document attachments in a manual step. For a secure SSL connection the keystore must be set using volume `/opt/nuclos/keystore` and password `KEYSTORE_PASSWORD`.

```shell
docker run -d nuclos:4.17.1 --name nuclos-pro-417 -p 4417:80 -e WEBCLIENT_PORT=4417  
-e DB_SCHEMA=production -e KEYSTORE_PASSWORD=GeHeIm  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-keystore:/opt/nuclos/keystore:ro  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-backups:/opt/nuclos/backups  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-documents:/opt/nuclos/home/data/documents  
-e TOTAL_RAM_GB=4 -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8  
--volumes-from nuclos-db-9.6 --link nuclos-db-9.6:POSTGRES
```

![WARNING](http://r.nuccess.de/warning32.png) **Do not run productive, test and development environments in the same Nuclos-DB instance.**
The restore process of a DockerBackup first deletes the contained schema if it exists. So, if you want to import a DockerBackup of production into a test environment, and these two run in the same Nuclos-DB instance, the production will be deleted!


## Test Environment  
###### for Nuclos v4.17.1
Here is the example of a test environment based on a restore of a productive environment. The special feature is the volume */opt/nuclos/restorescripts*

```shell
docker run --rm -d nuccess/nuclos:4.17.1 --name nuclos-tst-417 -p 5417:80 -e WEBCLIENT_PORT=5417  
-e DB_SCHEMA=tst417  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-backups:/opt/nuclos/backups  
-v /Users/Maik/Nuclos/Docker/vol-nuclos-restore:/opt/nuclos/restorescripts:ro  
-e DEVELOPMENT=true -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8  
--volumes-from nuclos-db-9.6 --link nuclos-db-9.6:POSTGRES
```

The Nuclos **DockerRestore** job restores a DockerBackup taken from production. After a restore the *.sql scripts in `/opt/nuclos/restorescripts` are executed automatically. Please also read the warning in the production environment section.

Useful examples after a restoration could be:

* Change Nuclos system paramter value of NUCLOS_INSTANCE_NAME
```sql
UPDATE T_AD_PARAMETER SET STRVALUE='TEST Instance', STRCHANGED='restorescript',  
DATCHANGED=clock_timestamp() WHERE STRPARAMETER='NUCLOS_INSTANCE_NAME';
```

* Change a Nuclet parameter to disable productive features, such as sending mails to customers
```sql
UPDATE T_MD_NUCLETPARAMETER SET STRVALUE='n', STRCHANGED='restorescript', DATCHANGED=clock_timestamp()  
WHERE STRPARAMETER='Produktivsystem';
```

* Stop all Nuclos jobs
```sql
DELETE FROM T_AD_QRTZ_FIRED_TRIGGERS;  
DELETE FROM T_AD_QRTZ_SIMPLE_TRIGGERS;  
DELETE FROM T_AD_QRTZ_CRON_TRIGGERS;  
DELETE FROM T_AD_QRTZ_TRIGGERS;  
UPDATE T_MD_JOBCONTROLLER SET STRJOBLASTSTATE='Deaktiviert', BLNRUNNING=false,  
STRCHANGED='restorescript', DATCHANGED=clock_timestamp();
```

* Remove user passwords
```sql
UPDATE T_MD_USER SET STRPASSWORD=null, STRCHANGED='restorescript', DATCHANGED=clock_timestamp();
```

* Disable LDAP
```sql
UPDATE T_AD_LDAPSERVER SET BLNACTIVE=false;
```

---

## Nuclos Clients

* ### Nuclos Webclient  
The Webclient runs on the specified port: `-p 8417:80 -e WEBCLIENT_PORT=8417`  
[http://localhost:8417](http://localhost:8417)  
As you can see, we have to specify the port two times, otherwise the Nuclos webclient can not connect to the server. In addition, this port must not be changed by forwarding rules in a router or similar.

* ### Nuclos Start Page (Welcome Page)  
For the start page, simply add `/nuclos` to the webclient URL  
[http://localhost:8417/nuclos](http://localhost:8417/nuclos)

* ### Nuclos Desktop Client (Java Webstart)  
On the start page you will find a start button.  
Alternatively, you can run the following command: `javaws "http://localhost:8417/nuclos/app/webstart.jnlp"`

* ### Default User  
The default user is `nuclos` with an empty password.

---

## Data Volumes

* ### `/opt/nuclos/backups`  
Directory for all backups made by the DockerBackup Job. For more information visit the [Wiki](https://bitbucket.org/nuccess/docker-nuclos/wiki/Home/)

* ### `/opt/nuclos/home/data/codegenerator`  
Contains generated Java source files. When development is enabled `-e DEVELOPMENT=true`, changes to rule sources (Directory *src*) are automatically detected and compiled by Nuclos.

* ### `/opt/nuclos/home/data/documents`  
Storage of Nuclos managed document attachments.

* ### `/opt/nuclos/home/data/index`  
Storage of Nuclos live search index files.

* ### `/opt/nuclos/home/logs`  
Nuclos server log files.

* ### `/opt/nuclos/extensions`  
Nuclos server and client extensions. Same use as with a Nuclos installer.

* ### `/opt/nuclos/keystore`  
Keystore file for a secure SSL connection between client and server. For more information see *Production Environment*.

* ### `/opt/nuclos/restorescripts`  
After a restore these scripts are executed automatically. For more information see *Test Environment*.

---

## Environment Variables

* ### DB_SCHEMA
Specify the name of the schema to use in the Nuclos-DB. The schema may be used only by one Nuclos application server at the same time. Default is "nuclos". `-e DB_SCHEMA=dev417`

* ### DEVELOPMENT
Enable development features such as external code changes (volume codegenerator), an open Java debug port at *8000*, Webclient rest explorer and some more. `-e DEVELOPMENT=true`  
For more information see *Development Environment*.

* ### KEYSTORE_PASSWORD
Set the password for the specified keystore under the data volume */opt/nuclos/keystore*. `-e KEYSTORE_PASSWORD=GeHeIm`

* ### LIVE_SEARCH
Set Nuclos live search enabled. Default is true. `-e LIVE_SEARCH=true`

* ### LOCALE
Set the locale. Default is de_DE.UTF-8. `-e LOCALE=de_DE.UTF-8`

* ### TOTAL_RAM_GB
Set total usable RAM for the Java process. Default is 2 GB. `-e TOTAL_RAM_GB=2`

* ### TZ
Set the timezone. Default is Europe/Berlin. `-e TZ=Europe/Berlin`

* ### WEBCLIENT_PORT
Please use the same port as in the mapping *-p 8417:80*. Default is 80 `-e WEBCLIENT_PORT=80`

---

## Further Notes

### Your Docker GUI does not support `--volumes-from`?
No problem, you can also set the interface volume manually in both containers. `-v /Users/Maik/Nuclos/Docker/vol-nuclos-db-9.6:/var/nuclos-db`