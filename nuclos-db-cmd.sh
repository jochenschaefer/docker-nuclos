#!/bin/bash

CMD_FILE=$POSTGRES_ENV_EXECUTE_CMD_DIR/$2

echo "$1" > $CMD_FILE
while true; do
	# wait until removed / executed
	if [ ! -f $CMD_FILE ]; then
		break
	fi
	sleep 2
done