#!/bin/bash

# Nuclos installer to use
NUCLOS_INSTALLER="$1"
echo "Update Nuclos $NUCLOS_INSTALLER: start..." >> $NUCLOS_HOME/logs/server.log

# Disable restart (keep-alive)
echo "nuclos-install.sh" > $NUCLOS_DIR/keep-alive.disabled

# Shutdown or kill Nuclos
nuclos-shutdown.sh

# Nuclos tomcat directory, remove old one before installation
TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)
if [ -n "$TOMCAT_DIR" ]; then
	rm -fdr "$TOMCAT_DIR"
fi

# Execute the installer in silent mode
$JAVA_HOME/bin/java -jar $NUCLOS_INSTALLER -s $NUCLOS_HOME/nuclos.xml 2>&1 >> $NUCLOS_HOME/logs/server.log
TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)

# make scripts runnable
chmod +x $NUCLOS_HOME/bin/*.sh
chmod +x $TOMCAT_DIR/bin/*.sh

# Backup properties for later config
rm -f $NUCLOS_DIR/server.properties-template
rm -f $NUCLOS_DIR/jdbc.properties-template
cp $NUCLOS_CONF/server.properties $NUCLOS_DIR/server.properties-template
cp $NUCLOS_CONF/jdbc.properties $NUCLOS_DIR/jdbc.properties-template

# Change Tomcat defaults, remove templates, manager, etc.
# Place a redirect in ROOT to Nuclos webclient.
# Backup config files for updating with parameters during run.
rm -fdr $(find $TOMCAT_DIR/webapps/* -maxdepth 0 ! -name ROOT -type d)
rm -f $(find $TOMCAT_DIR/webapps/ROOT -maxdepth 1 -type f)
echo "<% response.sendRedirect(\"/webclient\"); %>" > $TOMCAT_DIR/webapps/ROOT/index.jsp
mv $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF $TOMCAT_DIR/$NUCLOS_WEBCLIENT_CONF-template
mv $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF $TOMCAT_DIR/$NUCLOS_TOMCAT_CONF-template
mv $NUCLOS_STARTUP $NUCLOS_STARTUP-template

# Configure Nuclos installation
nuclos-config.sh

# Enable keep-alive again
rm -f $NUCLOS_DIR/keep-alive.disabled

echo "Update Nuclos $NUCLOS_INSTALLER: complete... wait for restart" >> $NUCLOS_HOME/logs/server.log