#!/bin/bash

# Set the locale
if [ -n "$LOCALE" ]; then
	GREP_LOCALE=`grep "^$LOCALE " /usr/share/i18n/SUPPORTED`
	if [ -n "$GREP_LOCALE" ]; then
		LOCALE_ENCODING=${GREP_LOCALE##* }
		LOCALE_WITHOUT_ENCODING=${LOCALE%.*}
		echo "Set locale to $LOCALE_WITHOUT_ENCODING.$LOCALE_ENCODING"
		localedef -i $LOCALE_WITHOUT_ENCODING -c -f $LOCALE_ENCODING -A /usr/share/locale/locale.alias $LOCALE_WITHOUT_ENCODING.$LOCALE_ENCODING
		export LANG=$LOCALE_WITHOUT_ENCODING.$LOCALE_ENCODING
		update-locale LANG=$LANG LC_MESSAGES=POSIX
	else
		echo "Locale \"$LOCALE\" is not supported. See list of available /usr/share/i18n/SUPPORTED:"
		cat /usr/share/i18n/SUPPORTED
	fi
fi

# Remove old install flag
rm -f $NUCLOS_DIR/nuclos.install

# Configure Nuclos
nuclos-config.sh

# Create schema if not exist
export DB_SCHEMA=${DB_SCHEMA:-'nuclos'}
nuclos-db-cmd.sh "CREATE SCHEMA IF NOT EXISTS $DB_SCHEMA AUTHORIZATION $POSTGRES_ENV_POSTGRES_USER" start-$DB_SCHEMA-schema.sql


SCRIPT_SHUTDOWN=0

# Install shutdown trap
function nucshutdown {
	SCRIPT_SHUTDOWN=1
	$NUCLOS_HOME/bin/shutdown.sh
}
trap nucshutdown EXIT

# Start function
function startAppServer {
	# Remove old logs, to identify the server start correctly
	rm -f $NUCLOS_HOME/logs/server*
	
	# Remove old tomcat logs
	TOMCAT_DIR=$(find $NUCLOS_HOME -iname "apache-tomcat-*" -type d)
	rm -f "$TOMCAT_DIR/logs/*"
	
	# Start the server
	$NUCLOS_HOME/bin/startup.sh
	
	while  [ ! -f $NUCLOS_HOME/logs/server.log ]; do
		sleep 0.4
	done

	# Create Docker Nuclet for import(update) after AutoDbSetup completed
	DOCKER_NUCLET="set search_path to $DB_SCHEMA;"
	DOCKER_NUCLET+=$'\n'
	for FILE in `find $NUCLOS_DIR -name "*.sql" -type f | sort -n`; do
		FILENAME="${FILE##*/}"
		DOCKER_NUCLET+=$(cat $NUCLOS_DIR/$FILENAME)
		DOCKER_NUCLET+=$'\n'
	done
	
	# Wait for server startup
	DBSETUP=0
	STARTED=0
	until [ ${STARTED} -eq 1 ]; do
		sleep 0.5
		if  [ ! -f $NUCLOS_HOME/logs/server.log ]; then
			# Shut down in progress...
			return 1
		fi
		while read LOG_LINE; do
			if [[ $LOG_LINE == *"Error auto db setup"* ]]; then
				# a good place for a future function: automatically restore to last data backup and downgrade with backup installer?
				sleep 5
				echo ">>> Error auto db setup <<<" >> $NUCLOS_HOME/logs/server.log
				BACKUP_INSTALLER=`find $NUCLOS_DIR/update -name nuclos-*-installer-generic.jar -print -quit`
				echo ">>> Try to restore a backup with:" >> $NUCLOS_HOME/logs/server.log
				echo "    nuclos-restore.sh $DB_SCHEMA#latest" >> $NUCLOS_HOME/logs/server.log
				echo ">>> And/or try to install the last Nuclos version with:" >> $NUCLOS_HOME/logs/server.log
				echo "    nuclos-install.sh $BACKUP_INSTALLER" >> $NUCLOS_HOME/logs/server.log
				return 1
			fi
			if [ ${DBSETUP} -eq 0 ] && ( [[ $LOG_LINE == *"AutoDbSetupComplete"* ]] || [[ $LOG_LINE == *"Auto-Init finished successfully"* ]] || [[ $LOG_LINE == *"Auto-Update finished successfully"* ]]; ) then
				echo "Nuclos AutoDbSetup completed"
				DBSETUP=1
				# Install or update Docker Nuclet
				nuclos-db-cmd.sh "$DOCKER_NUCLET" start-$DB_SCHEMA-docker-nuclet.sql
				# Do something after AutoDbSetup
				# ...
			fi
			if [[ $LOG_LINE == *"Starting Quartz Scheduler now"* ]]; then
				echo "Nuclos startup completed" >> $NUCLOS_HOME/logs/server.log
				STARTED=1
				# Do something after server startup
				# ...
			fi
		done < $NUCLOS_HOME/logs/server.log
	done
	sleep 10
	return 0
}

# tail server log outputs
nuclos-logs.sh &


startAppServer

# Keep nuclos alive and look for restarts, restores, updates etc.
rm -f $NUCLOS_DIR/keep-alive.disabled
while true; do
	if [ ${SCRIPT_SHUTDOWN} -eq 1 ]; then
		exit 0
	fi
	nc -z -w5 127.0.0.1 $NUCLOS_PORT < /dev/null
	if [ $? -ne 0 ]; then
		# Nuclos not running any more, restart
		if [ ${SCRIPT_SHUTDOWN} -eq 0 ]; then
			nuclos-shutdown.sh
		fi
		if [ ${SCRIPT_SHUTDOWN} -eq 0 ]; then
			if [ ! -f $NUCLOS_DIR/keep-alive.disabled ]; then
				startAppServer
			fi
		fi
		sleep 30
	fi
	if [ -f $NUCLOS_DIR/nuclos.restart ]; then
		# Restart only
		echo "Restart Nuclos..."
		rm $NUCLOS_DIR/nuclos.restart
		# wait for the job to finish
		sleep 5
		nuclos-shutdown.sh
	fi
	if [ -f $NUCLOS_DIR/backup.restore ]; then
		# Restore a backup
		RESTORE_NAME=$(<$NUCLOS_DIR/backup.restore)
		rm $NUCLOS_DIR/backup.restore
		# wait for the job to finish
		sleep 5
		nuclos-restore.sh "$RESTORE_NAME"
	fi
	if [ -f $NUCLOS_DIR/nuclos.update ]; then
		# Update Nuclos
		NUCLOS_INSTALLER=$(<$NUCLOS_DIR/nuclos.update)
		rm $NUCLOS_DIR/nuclos.update
		# wait for the job to finish
		sleep 5
		nuclos-install.sh "$NUCLOS_INSTALLER"
		if [ -f $NUCLOS_DIR/update/job-session-id.txt ]; then
			JOB_SESSION_ID=$(<$NUCLOS_DIR/update/job-session-id.txt)
			JOB_LOG_SQL="set search_path to $DB_SCHEMA;"
			JOB_LOG_SQL+=$'\n'
			JOB_LOG_SQL+="INSERT INTO T_MD_JOBRUN_MESSAGES (INTID, INTID_T_MD_JOBRUN, STRMESSAGELEVEL, STRMESSAGE, STRRULE, DATCREATED, STRCREATED, DATCHANGED, STRCHANGED, INTVERSION) VALUES (NEXTVAL('IDFACTORY'), $JOB_SESSION_ID, 'INFO', 'Update complete', 'DockerCore', clock_timestamp(), USER, clock_timestamp(), USER, 1);"
			nuclos-db-cmd.sh "$JOB_LOG_SQL" "nuclos-update-complete-$DB_SCHEMA.sql"
			rm -f $NUCLOS_DIR/update/job-session-id.txt
		fi
	fi
	sleep 10
done