#!/bin/bash

# Disable restart (keep-alive)
echo "nuclos-restore.sh" > $NUCLOS_DIR/keep-alive.disabled

cd $NUCLOS_DIR/backups
RESTORE_NAME="$1"
RESTORE_DUMP_FILE=`find $NUCLOS_DIR/backups/$RESTORE_NAME -name *backup.tar.gz -print -quit`
RESTORE_DUMP_FILENAME="${RESTORE_DUMP_FILE##*/}"
RESTORE_DUMP_NAME="${RESTORE_DUMP_FILENAME%.backup.tar.gz}"  # without extension
RESTORE_BACKUP_SCHEMA=${RESTORE_DUMP_NAME%\#*}  # retain the part before the number sign
BACKUP_DIR=$NUCLOS_DIR/backups/$RESTORE_NAME

$NUCLOS_HOME/bin/shutdown.sh
sleep 10
pkill -9 -f java
echo "Restore $RESTORE_NAME: import dump $RESTORE_DUMP_FILE..." >> $NUCLOS_HOME/logs/server.log
cp -f $RESTORE_DUMP_FILE $POSTGRES_ENV_EXECUTE_CMD_BACKUPS_DIR
cd $POSTGRES_ENV_EXECUTE_CMD_BACKUPS_DIR
tar xzf $RESTORE_DUMP_FILE
rm $POSTGRES_ENV_EXECUTE_CMD_BACKUPS_DIR/$RESTORE_DUMP_FILENAME
nuclos-db-cmd.sh "Restore" $RESTORE_DUMP_NAME.imp
if [ "$RESTORE_BACKUP_SCHEMA" != "$DB_SCHEMA" ]; then
	echo "Restore $RESTORE_NAME: rename schema $RESTORE_BACKUP_SCHEMA to $DB_SCHEMA..." >> $NUCLOS_HOME/logs/server.log
	nuclos-db-cmd.sh "DROP SCHEMA $DB_SCHEMA CASCADE" "drop-$DB_SCHEMA.sql"
	nuclos-db-cmd.sh "ALTER SCHEMA $RESTORE_BACKUP_SCHEMA RENAME TO $DB_SCHEMA" "rename-$RESTORE_BACKUP_SCHEMA-to-$DB_SCHEMA.sql"
fi
if  [ -d $BACKUP_DIR/extensions ]; then
	echo "Restore $RESTORE_NAME: restore extensions..." >> $NUCLOS_HOME/logs/server.log
	mkdir -p $NUCLOS_DIR/extensions
	rm -fdr $NUCLOS_DIR/extensions/*
	cp -r $BACKUP_DIR/extensions/* $NUCLOS_DIR/extensions/
	nuclos-extensions.sh
fi
if  [ -d $BACKUP_DIR/documents ]; then
	echo "Restore $RESTORE_NAME: restore documents..." >> $NUCLOS_HOME/logs/server.log
	mkdir -p $NUCLOS_HOME/data/documents
	rm -fdr $NUCLOS_HOME/data/documents/*
	cp -r $BACKUP_DIR/documents/* $NUCLOS_HOME/data/documents/
fi
if  [ -d $NUCLOS_DIR/restorescripts ]; then
	for FILE in `find $NUCLOS_DIR/restorescripts -name "*.sql" -type f | sort -n`; do
		FILENAME="${FILE##*/}"
		RESTORE_SQL="set search_path to $DB_SCHEMA;"
		RESTORE_SQL+=$'\n'
		RESTORE_SQL+=$(cat $NUCLOS_DIR/restorescripts/$FILENAME)
		RESTORE_SQL+=$'\n'
		echo "Restore $RESTORE_NAME: execute restore script $FILENAME..." >> $NUCLOS_HOME/logs/server.log
		nuclos-db-cmd.sh "$RESTORE_SQL" restore-$DB_SCHEMA-run-script-$FILENAME
	done
fi
echo "Restore $RESTORE_NAME: complete... wait for restart" >> $NUCLOS_HOME/logs/server.log

# Enable keep-alive again, but only if no nuclos install is in queue
if [ ! -f $NUCLOS_DIR/nuclos.update ]; then
	rm -f $NUCLOS_DIR/keep-alive.disabled
fi