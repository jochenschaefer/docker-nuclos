#!/bin/bash

echo "Install Nuclos extensions..." >> $NUCLOS_HOME/logs/server.log
if  [ -d $NUCLOS_HOME/webapp/WEB-INF/lib/extensions ]; then
	echo "Remove old extensions..." >> $NUCLOS_HOME/logs/server.log
	cd $NUCLOS_HOME/webapp/WEB-INF/lib
	rm -f $(cd $NUCLOS_HOME/webapp/WEB-INF/lib/extensions && find . -maxdepth 1 -type f)
fi
rm -fdr $NUCLOS_HOME/webapp/WEB-INF/lib/extensions
rm -fdr $NUCLOS_HOME/webapp/app/extensions
mkdir -p $NUCLOS_HOME/webapp/WEB-INF/lib/extensions
mkdir -p $NUCLOS_HOME/webapp/app/extensions/themes
if  [ -d $NUCLOS_DIR/extensions/server ]; then
	echo "Copy server extensions..." >> $NUCLOS_HOME/logs/server.log
	cp $NUCLOS_DIR/extensions/server/*.jar* $NUCLOS_HOME/webapp/WEB-INF/lib
	cp $NUCLOS_DIR/extensions/server/*.jar* $NUCLOS_HOME/webapp/WEB-INF/lib/extensions
fi
if  [ -d $NUCLOS_DIR/extensions/client ]; then
	echo "Copy client extensions..." >> $NUCLOS_HOME/logs/server.log
	cp $NUCLOS_DIR/extensions/client/*.jar* $NUCLOS_HOME/webapp/app/extensions
	if  [ -d $NUCLOS_DIR/extensions/client/themes ]; then
		cp $NUCLOS_DIR/extensions/client/*.jar* $NUCLOS_HOME/webapp/app/extensions/themes
	fi
fi
if  [ -d $NUCLOS_DIR/extensions/common ]; then
	echo "Copy common extensions..." >> $NUCLOS_HOME/logs/server.log
	cp $NUCLOS_DIR/extensions/common/*.jar* $NUCLOS_HOME/webapp/WEB-INF/lib
	cp $NUCLOS_DIR/extensions/common/*.jar* $NUCLOS_HOME/webapp/WEB-INF/lib/extensions
	cp $NUCLOS_DIR/extensions/common/*.jar* $NUCLOS_HOME/webapp/app/extensions
fi

echo "Copy NuclosDocker extension..." >> $NUCLOS_HOME/logs/server.log
cp -f $NUCLOS_DIR/Docker*.jar $NUCLOS_HOME/webapp/WEB-INF/lib