INSERT INTO t_md_servercode (struid, datcreated, strcreated, datchanged, strchanged, intversion, strdescription, strname, blnactive, clbsource, struid_t_md_nuclet, blndebug) VALUES ('P8uwREJZoVx8ZUROvV1X', clock_timestamp(), 'DockerAutoSetup', clock_timestamp(), 'DockerAutoSetup', 0, 'Backup the current schema', 'de.nuccess.docker.rule.DockerBackupJob', true, 'package de.nuccess.docker.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.JobContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.JobRule;

import de.nuccess.docker.DockerCore;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DockerBackupJob", description="Backup the current schema, extensions and documents")
public class DockerBackupJob implements JobRule {

	public void execute(JobContext context) throws BusinessException {
		DockerCore.backupInstance(context);
	}

}', 'bO3OU7JmxSTNePmo02RZ', false);
INSERT INTO t_md_servercode (struid, datcreated, strcreated, datchanged, strchanged, intversion, strdescription, strname, blnactive, clbsource, struid_t_md_nuclet, blndebug) VALUES ('qV57bBEPP9O6aoZdYlGh', clock_timestamp(), 'DockerAutoSetup', clock_timestamp(), 'DockerAutoSetup', 0, 'Restore a backup and restart', 'de.nuccess.docker.rule.DockerRestoreJob', true, 'package de.nuccess.docker.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.JobContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.JobRule;

import de.nuccess.docker.DockerCore;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DockerRestoreJob", description="Restore a backup and restart")
public class DockerRestoreJob implements JobRule {

	public void execute(JobContext context) throws BusinessException {
		DockerCore.restoreInstance(context);
	}

}', 'bO3OU7JmxSTNePmo02RZ', false);
INSERT INTO t_md_servercode (struid, datcreated, strcreated, datchanged, strchanged, intversion, strdescription, strname, blnactive, clbsource, struid_t_md_nuclet, blndebug) VALUES ('SuHBjCBotrMsnVwacAkS', clock_timestamp(), 'DockerAutoSetup', clock_timestamp(), 'DockerAutoSetup', 0, 'Restart the application server', 'de.nuccess.docker.rule.DockerRestartJob', true, 'package de.nuccess.docker.rule; 

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.JobContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.JobRule;

import de.nuccess.docker.DockerCore; 

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DockerRestartJob", description="Restart the application server")
public class DockerRestartJob implements JobRule {

	public void execute(JobContext context) throws BusinessException {
		DockerCore.restartAppServer(context);
	}

}', 'bO3OU7JmxSTNePmo02RZ', false);
INSERT INTO t_md_servercode (struid, datcreated, strcreated, datchanged, strchanged, intversion, strdescription, strname, blnactive, clbsource, struid_t_md_nuclet, blndebug) VALUES ('pXgxgykp3bYS5eH8ivJj', clock_timestamp(), 'DockerAutoSetup', clock_timestamp(), 'DockerAutoSetup', 0, 'Update Nuclos', 'de.nuccess.docker.rule.DockerUpdateJob', true, 'package de.nuccess.docker.rule; 

import org.nuclos.api.rule.JobRule; 
import org.nuclos.api.context.JobContext; 
import org.nuclos.api.annotation.Rule; 
import org.nuclos.api.exception.BusinessException; 

import de.nuccess.docker.DockerCore;

/** @name        
  * @description 
  * @usage       
  * @change      
*/
@Rule(name="DockerUpdateJob", description="Update Nuclos")
public class DockerUpdateJob implements JobRule {

	public void execute(JobContext context) throws BusinessException {
		DockerCore.updateNuclos(context);
	}
}', 'bO3OU7JmxSTNePmo02RZ', false);
